<?php include_once "app/SQLConnection.php"; ?>

<?php include_once "./header.php" ?>
<div class="container-fluid my-2">
    <?php
    $connection = new SQLConnection();
    ?>
    <h2>How it Works</h2>
    <dl>
        <dt>Sign Up</dt>
        <dd>Sign Up if you haven't already</dd>
        <dt>Submit Rankings</dt>
        <dd>Review the cards and submit your rankings. Confirm you've selected the correct User before submitting. <b>It is a full sync and will not autosave!</b></dd>
        <dt>Make Bets</dt>
        <dd>View the rankings once everyone has submitted and make bets!</dd>
    </dl>

    <h2>Bets</h2>
    <h6>Set: <?php echo strtoupper(DbConfig::ACTIVE_SET); ?> </h6>
    <div class="table-responsive">
        <table class="table table-striped">
            <thead>
                <tr>
                    <td>Card</td>
                    <td>Betting Lower</td>
                    <td>Lower Rating</td>
                    <td>Higher Rating </td>
                    <td>Betting Higher</td>
                </tr>
            </thead>
            <tbody>
                <?php
                $bets = $connection->getBets();
                if ($bets != 0) {
                    foreach ($bets as $bet) {
                        echo "<tr><td>${bet['cardName']}</td><td>${bet['lowSide']}</td><td>${bet['low']}</td>";
                        echo "<td>${bet['high']}</td><td>${bet['highSide']}</td></tr>";
                    }
                }
                ?>
            </tbody>
        </table>
    </div>

</div>
<?php include_once "./footer.php" ?>