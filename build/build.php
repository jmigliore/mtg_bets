<?php

include_once $_SERVER['DOCUMENT_ROOT'] . "/mtg_bets/app/SQLConnection.php";
include_once $_SERVER['DOCUMENT_ROOT'] . '/mtg_bets/app/DbConfig.php';

$set = DbConfig::ACTIVE_SET;

getMtgSet($set);

SQLConnection::buildDatabase();
SQLConnection::addMtgSet($set);

function getMtgSet($set)
{
    //Create JSON file for set data
    $searchAPI = 'https://api.scryfall.com/cards/search?q=';
    $q = urlencode("set:" . $set);
    $url = $searchAPI . $q;

    echo "Request: " . $url . "<br>";

    $continue = true;
    $cards = [];
    $notBoosterCard = 0;

    while ($continue) {
        $response = makeCall($url);

        $data = $response['data'];
        $nextPage = $response['next_page'];
        //echo "<pre>";print_r($response);
        if (sizeof($cards) == 0) {
            echo "Found " . $response['total_cards'] . " cards.<br>";
        }

        if (is_array($data)) {
            foreach ($data as $card) {
                if ($card['booster'] == true) {
                    $json = array(
                        'set' => $set,
                        'id' => $card['id'],
                        'name' => $card['name'],
                        'colors' => $card['colors'],
                        'type' => $card['type_line'],
                        'rarity' => $card['rarity'],
                        'collector_number' => $card['collector_number']
                    );

                    if (isset($card['card_faces'])) {
                        $cardFaces = [];
                        $cardFaces[0]['colors'] = $card['card_faces'][0]['colors'];
                        $cardFaces[1]['colors'] = $card['card_faces'][1]['colors'];

                        $json['card_faces'] = $cardFaces;
                    }
                    array_push($cards, $json);
                } else {
                    echo "Non-Booster card: " . $card['name'] . "<br>";
                    $notBoosterCard++;
                }
            }
        }


        if ($nextPage == null) {
            $continue = false;
        } else {
            $url = $nextPage;
        }
    }

    $fh = fopen("../db/" . $set . ".json", "w") or die("Unable to open " . $set . ".json");
    $bytes = fwrite($fh, json_encode($cards, JSON_PRETTY_PRINT));
    fclose($fh);
    echo "Found $notBoosterCard non-booster cards. <br>";

    if ($bytes != false) {
        echo "Created " . $set . ".json with " . sizeof($cards) . " cards (remove non-booster cards).<br>";
    } else {
        echo "<strong>Error creating " . $set . ".json.<strong><br>";
        exit;
    }
}

//Make API GET call to Scryfall
function makeCall($url)
{
    $ch = curl_init($url);

    curl_setopt($ch, CURLOPT_HTTPGET, true);
    curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);

    $response_json = curl_exec($ch);

    curl_close($ch);

    return json_decode($response_json, true);
}
