-- MySQL Workbench Forward Engineering

SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0;
SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0;
SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='TRADITIONAL,ALLOW_INVALID_DATES';

-- -----------------------------------------------------
-- Schema mtg_bets
-- -----------------------------------------------------

-- -----------------------------------------------------
-- Schema mtg_bets
-- -----------------------------------------------------
CREATE SCHEMA IF NOT EXISTS `mtg_bets` DEFAULT CHARACTER SET utf8 ;
USE `mtg_bets` ;

-- -----------------------------------------------------
-- Table `mtg_bets`.`player`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `mtg_bets`.`player` (
  `idplayer` INT NOT NULL AUTO_INCREMENT,
  `firstName` VARCHAR(45) NOT NULL,
  `lastName` VARCHAR(45) NOT NULL,
  PRIMARY KEY (`idplayer`))
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `mtg_bets`.`card`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `mtg_bets`.`card` (
  `idcard` INT NOT NULL AUTO_INCREMENT,
  `scryID` VARCHAR(255) NOT NULL,
  `mtgSet` VARCHAR(45) NOT NULL,
  `cardName` VARCHAR(255) NOT NULL,
  `rarity` VARCHAR(45) NOT NULL,
  `collectorNumber` INT NOT NULL,
  `cardType` VARCHAR(255) NOT NULL,
  `colors` VARCHAR(45) NULL,
  `doubleFaced` TINYINT(1) UNSIGNED NULL,
  PRIMARY KEY (`idcard`),
  UNIQUE INDEX `scryID_UNIQUE` (`scryID` ASC))
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `mtg_bets`.`ranking`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `mtg_bets`.`ranking` (
  `idranking` INT NOT NULL AUTO_INCREMENT,
  `rankingValue` DECIMAL(2,1) NOT NULL,
  `card_idcard` INT NOT NULL,
  `player_idplayer` INT NOT NULL,
  PRIMARY KEY (`idranking`),
  INDEX `card_idcard_idx` (`card_idcard` ASC),
  INDEX `player_idplayer_idx` (`player_idplayer` ASC),
  UNIQUE INDEX `KEY` (`card_idcard` ASC, `player_idplayer` ASC),
  CONSTRAINT `ranking_idcard`
    FOREIGN KEY (`card_idcard`)
    REFERENCES `mtg_bets`.`card` (`idcard`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
  CONSTRAINT `ranking_idplayer`
    FOREIGN KEY (`player_idplayer`)
    REFERENCES `mtg_bets`.`player` (`idplayer`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `mtg_bets`.`bet`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `mtg_bets`.`bet` (
  `idbet` INT NOT NULL AUTO_INCREMENT,
  `card_idcard` INT NOT NULL,
  `high` DECIMAL(2,1) NOT NULL,
  `low` DECIMAL(2,1) NOT NULL,
  `high_is_winner` TINYINT(1) UNSIGNED NULL,
  `low_is_winner` TINYINT(1) UNSIGNED NULL,
  PRIMARY KEY (`idbet`),
  INDEX `card_idcard_idx` (`card_idcard` ASC),
  CONSTRAINT `bet_idcard`
    FOREIGN KEY (`card_idcard`)
    REFERENCES `mtg_bets`.`card` (`idcard`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `mtg_bets`.`bet_low_side`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `mtg_bets`.`bet_low_side` (
  `idbet_low_side` INT NOT NULL AUTO_INCREMENT,
  `bet_idbet` INT NOT NULL,
  `player_idplayer` INT NOT NULL,
  INDEX `player_idplayer_idx` (`player_idplayer` ASC),
  INDEX `bet_idbet_idx` (`bet_idbet` ASC),
  UNIQUE INDEX `KEY` (`bet_idbet` ASC, `player_idplayer` ASC),
  PRIMARY KEY (`idbet_low_side`),
  CONSTRAINT `bet_low_side_idplayer`
    FOREIGN KEY (`player_idplayer`)
    REFERENCES `mtg_bets`.`player` (`idplayer`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
  CONSTRAINT `bet_low_side_idbet`
    FOREIGN KEY (`bet_idbet`)
    REFERENCES `mtg_bets`.`bet` (`idbet`)
    ON DELETE CASCADE
    ON UPDATE NO ACTION)
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `mtg_bets`.`bet_high_side`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `mtg_bets`.`bet_high_side` (
  `idbet_high_side` INT NOT NULL AUTO_INCREMENT,
  `bet_idbet` INT NOT NULL,
  `player_idplayer` INT NOT NULL,
  INDEX `bet_idbet_idx` (`bet_idbet` ASC),
  INDEX `player_idplayer_idx` (`player_idplayer` ASC),
  UNIQUE INDEX `KEY` (`bet_idbet` ASC, `player_idplayer` ASC),
  PRIMARY KEY (`idbet_high_side`),
  CONSTRAINT `bet_high_side_idbet`
    FOREIGN KEY (`bet_idbet`)
    REFERENCES `mtg_bets`.`bet` (`idbet`)
    ON DELETE CASCADE
    ON UPDATE NO ACTION,
  CONSTRAINT `bet_high_side_idplayer`
    FOREIGN KEY (`player_idplayer`)
    REFERENCES `mtg_bets`.`player` (`idplayer`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB;


SET SQL_MODE=@OLD_SQL_MODE;
SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS;
SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS;
