<?php

include $_SERVER['DOCUMENT_ROOT'] . '/mtg_bets/app/DbConfig.php';

Class SQLConnection {

    public $pdo;
    private $config;

    public function __construct($build = false) {
        $this->config = new DbConfig;
        $this->connect($build);
    }

    public function connect($build = false) {
        if ($this->pdo == null) {
            if ($build) {
                $dsn = $this->getDSN();
            } else {
                $dsn = $this->getDSN($this->config->dbName);
            }
            $this->pdo = new PDO($dsn, $this->config->userName, $this->config->password);
        }
        $this->pdo->setAttribute(PDO::ATTR_DEFAULT_FETCH_MODE, PDO::FETCH_ASSOC); //remove extra keys in results
        $this->pdo->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION); //set error mode
    }

    public function getDSN($dbName = null) {
        $dsn = "mysql:host=" . $this->config->serverName;

        if ($dbName != null) {
            $dsn .= ";dbname=" . $this->config->dbName;
        }
        return $dsn;
    }

    public static function buildDatabase() {
        $connection = (new SQLConnection(true))->pdo;

        $sql = file_get_contents("../build/create_tables.sql");
        if ($sql === false) {
            echo "Could not load SQL script.<br>";
            exit;
        }

        $results = $connection->exec($sql);
        if ($results != 0 || $connection->errorInfo()[0] != 0) {
            echo "Error creating database.<br>";
            exit;
        } else {
            echo "Created database.<br>";
        }
    }

    public static function addMtgSet($set) {
        //add cards to db from json
        $connection = (new SQLConnection())->pdo;

        $cards = json_decode(file_get_contents("../db/" . $set . ".json"), true);

        $sql = $connection->prepare("INSERT INTO card (scryID, mtgSet, cardName, rarity, collectorNumber, cardType, colors, doubleFaced) VALUES (?, ?, ?, ?, ?, ?, ?, ?)");

        echo "Adding cards to database from json file.<br>";

        foreach ($cards as $card) {


            //Merge color array into string
            if (isset($card['card_faces'])) {
                $frontColor = $card['card_faces'][0]['colors'];
                $backColor = $card['card_faces'][1]['colors'];

                if ($frontColor && $backColor) {
                    $colors = implode("", array_unique(array_merge($frontColor, $backColor)));
                } else if ($frontColor) {
                    $colors = implode("", $frontColor);
                } else if($backColor){
                    $colors = implode("", $backColor);
                }
                $doubleFaced = 1;
            } else {
                $colors = $card['colors'] ? implode("", $card['colors']) : "";
                $doubleFaced = 0;
            }
            try {
                $result = $sql->execute([$card['id'], $set, $card['name'], $card['rarity'], $card['collector_number'], $card['type'], $colors, $doubleFaced]);
                
                if ($result == 1) {
                    echo "Added Card: " . $card['name'] . "<br>";
                } else {
                    echo "Error Adding Card.<br>";
                }
            } catch (PDOException $e){
                if(strpos($e->getMessage(), "Duplicate entry")){
                    echo "Duplicate Entry: " . $card['name'] . "<br>";
                }else{
                    //print_r($e);
                    echo "Error Adding: "  . $card['name']  . "[" .$e->getMessage() . "]<br>";

                }
            }

        }
    }

    public function addNewPlayer($firstName, $lastName) {
        $statement = 'INSERT INTO player 
                    (firstName, lastName)
                    VALUES
                    ("' . $firstName . '","' . $lastName . '");';
        return $this->pdo->exec($statement);
    }

    public function getPlayers() {
        $statement = 'SELECT 
                    player.idplayer,
                    concat(player.firstName, " ", player.lastName) as name
                    FROM player
                    ORDER BY LOWER(player.firstname);';
        return $this->pdo->query($statement)->fetchAll();
    }

    public function getCardIds($set = DbConfig::ACTIVE_SET) {
        $statement = 'SELECT card.idcard, card.scryID, card.doubleFaced FROM card WHERE mtgSet = "' . $set . '" ORDER BY card.collectorNumber';
        return $this->pdo->query($statement)->fetchAll();
    }

    public function addCardRanking($playerID, $rankings) {
        $statement = 'INSERT INTO ranking (player_idplayer, card_idcard, rankingValue) VALUES ';

        foreach ($rankings as $index => $ranking) {
            $index === key($rankings) ?: $statement .= ",";

            $statement .= "({$playerID},{$ranking['cardID']},{$ranking['ranking']})";
        }
        $statement .= " ON DUPLICATE KEY UPDATE rankingValue = VALUES(rankingValue)";
        return $this->pdo->exec($statement);
    }

    public function getCardRankings($playerID, $set = DbConfig::ACTIVE_SET) {
        if (is_numeric($playerID) && is_string($set)) {
            $statement = $this->pdo->prepare('SELECT ranking.card_idcard, ranking.rankingValue '
                    . 'FROM ranking INNER JOIN card ON card.idcard = ranking.card_idcard '
                    . 'WHERE ranking.player_idplayer = ? AND card.mtgSet = ?');

            $result = $statement->execute([$playerID, $set]);
            return $statement->fetchAll();
        } else {
            return "Invalid PlayerID or Set.";
        }
    }

    public function getRankings($set = DbConfig::ACTIVE_SET) {
        if (is_string($set)) {
            $statement = $this->pdo->prepare('SELECT card.idcard, card.cardName, card.scryID, card.doubleFaced, concat(player.firstName, " ", player.lastName) as name, player.idplayer, ranking.rankingValue, bet.idbet '
                    . 'FROM ranking INNER JOIN card ON card.idcard = ranking.card_idcard '
                    . 'INNER JOIN player ON player.idplayer = ranking.player_idplayer '
                    . 'LEFT JOIN bet on bet.card_idcard = ranking.card_idcard '
                    . 'WHERE card.mtgSet = ? '
                    . 'ORDER BY card.collectorNumber');
            $result = $statement->execute([$set]);
            return $statement->fetchAll();
        } else {
            return "Invalid Set.";
        }
    }

    public function addBet($cardID, $high, $low, $lowSide, $highSide) {
        $statement = $this->pdo->prepare('INSERT INTO bet (card_idcard, high, low) VALUES (?, ?, ?)');
        if ($statement->execute([$cardID, $high, $low])) {
            $betID = $this->pdo->lastInsertId();
            $success = true;
            foreach ($lowSide as $playerID) {
                $statement = $this->pdo->prepare('INSERT INTO bet_low_side (bet_idbet, player_idplayer) VALUES (?,?)');
                if (!$statement->execute([$betID, $playerID])) {
                    $success = false;
                }
            }
            foreach ($highSide as $playerID) {
                $statement = $this->pdo->prepare('INSERT INTO bet_high_side (bet_idbet, player_idplayer) VALUES (?,?)');
                if (!$statement->execute([$betID, $playerID])) {
                    $success - false;
                }
            }
            if ($betID && $success) {
                return 1;
            } else {
                return 0;
            }
        }
    }

    public function getBets($set = DbConfig::ACTIVE_SET) {
        $statement = $this->pdo->prepare("select bet.card_idcard, high, low, card.cardName, bet.idbet, group_concat(distinct high.firstName) as highSide, group_concat(distinct low.firstName) as lowSide from bet "
                . "inner join card on card.idcard = bet.card_idcard "
                . "right join bet_high_side on bet_high_side.bet_idbet = bet.idbet "
                . "left join bet_low_side on bet_low_side.bet_idbet = bet.idbet "
                . "left join player high on high.idplayer = bet_high_side.player_idplayer "
                . "left join player low on low.idplayer = bet_low_side.player_idplayer "
                . "where card.mtgSet = ? "
                . "group by bet.idbet "
                . "order by card.collectorNumber");
        if ($statement->execute([$set])) {
            return $statement->fetchAll();
        } else {
            return 0;
        }
    }

}
