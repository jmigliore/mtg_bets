<?php include_once "./header.php" ?>

<div class="container my-2">
    <h2>Sign Up for MTG Bets</h2>
    <form role="form" id="new-player-form">
        <div class="form-row">
            <div class="col-6">
                <label class="p-2">First Name</label>
                <input type="text" class="form-control" id="firstName" placeholder="First name" required>
            </div>
            <div class="col-6">
                <label class="p-2">Last Name</label>
                <input type="text" class="form-control" id="lastName" placeholder="Last name" required>
            </div>
        </div>
        <div id="feedback" role="alert"></div>
        <button type="submit" class="btn btn-primary mt-2" id="submit">Submit</button>
    </form>
</div>
<script type="text/javascript">
    $(document).ready(function () {
        //Ajax
        var submitted = false;
        $('#new-player-form').on('submit', function (event) {

            var data = {};

            data.service = 'NEW_PLAYER';
            data.firstName = $('input[id=firstName]').val();
            data.lastName = $('input[id=lastName]').val();

            if (!submitted) {
                submitted = true;

                $.ajax({
                    type: 'POST',
                    data: data,
                    url: './js/ajax-handler.php',
                    dataType: 'json',
                    error: function (jqXHR, textStatus, errorThrown) {},
                    success: function (response, textStatus, jqXHR) {
                        if (response.error) {
                            alert(response.error);
                            submitted = false;
                        } else {
                            var html = "<div class='col-sm-12 text-center'><h1>Welcome to the MTG Bets you degenerate!</h1></div>";
                            $('#new-player-form').html(html);
                        }
                    },
                    complete: function (jqXHR, textStatus) {}
                });

            }
            return false;

        });
    });
</script>
<?php include_once "./footer.php" ?>