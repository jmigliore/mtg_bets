<?php

include_once $_SERVER['DOCUMENT_ROOT'] . "/mtg_bets/app/SQLConnection.php";

//echo json_encode($_POST);
if (sizeof($_POST) > 0) {
    $response['service'] = empty($_GET['service']) ? (empty($_POST['service']) ? FALSE : $_POST['service']) : $_GET['service'];

    $data = cleanInput();
    switch ($response['service']) {
        case 'SUBMIT_RANKINGS':
            $response = submitRankings($data);
            break;
        case 'GET_RANKINGS':
            $response = getRankings($data);
            break;
        case 'SUBMIT_BET':
            $response = bet($data);
            break;
        case 'RESOLVE_BET':
            $response = resolveBet($data);
            break;
        case 'NEW_PLAYER':
            addNewPlayer($data);
            break;
        default:
            $response['error'] = 'Invalid AJAX service requested: "' . $response['service'] . '"';
    }
    echo json_encode($response);
} else {
    exit;
}

function addNewPlayer($data) {
    $connection = new SQLConnection();
    $connection->addNewPlayer($data['firstName'], $data['lastName']);
}

function bet($data) {
    if (!$data['cardID']) {
        $response['error'] = "Card ID missing";
    } else if (!is_numeric($data['lowBet']) || !is_numeric($data['highBet'])) {
        $response['error'] = "Bets are incorrect";
    } else if (!isset($data['lowSide']) || !isset($data['highSide'])) {
        $response['error'] = "Must have bets on each side";
    } else if ($response['error']) {
        return $response;
    } else {
        $connection = new SQLConnection();
        $response = $connection->addBet($data['cardID'], $data['highBet'], $data['lowBet'], $data['lowSide'], $data['highSide']);
        if ($response === 0) {
            $response['error'] = "Something went wrong.";
        }
    }
    return $response;
}

function submitRankings($data) {

    if (is_numeric($data['playerID'])) {
        $connection = new SQLConnection();
        $rankings = [];

        foreach ($data['cards'] as $card) {
            if (is_numeric($card['cardID']) && is_numeric($card['ranking'])) {
                array_push($rankings, array('cardID' => $card['cardID'], 'ranking' => $card['ranking']));
            }
        }
        $response = $connection->addCardRanking($data['playerID'], $rankings);
        setcookie("playerID", $data['playerID'], strtotime('+ 1 year'), "/mtg_bets");
    } else {
        $response['error'] = "PlayerID not found"; //doesn't check database
    }

    return $response;
}

function getRankings($data) {
    $playerID = $data['playerID'];
    if (is_numeric($playerID)) {
        $connection = new SQLConnection();
        $response = $connection->getCardRankings($playerID, DbConfig::ACTIVE_SET);
    }
    return $response;
}

function cleanInput() {
    $whitelist = array(
        'firstName',
        'lastName',
        'playerID',
        'cardID',
        'lowBet',
        'highBet'
    );

    foreach ($whitelist as $key) {
        if (isset($_POST[$key])) {
            $data[$key] = htmlspecialchars($_POST[$key]);
        }
    }

    $data['cards'] = isset($_POST['cards']) ? $_POST['cards'] : null;
    $data['highSide'] = isset($_POST['highSide']) ? $_POST['highSide'] : null;
    $data['lowSide'] = isset($_POST['lowSide']) ? $_POST['lowSide'] : null;

    return $data;
}

?>