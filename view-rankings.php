<?php include_once "./header.php" ?>
<?php include_once $_SERVER['DOCUMENT_ROOT'] . "/mtg_bets/app/SQLConnection.php"; ?>

<div class="container-fluid my-2">
    <?php
    $connection = new SQLConnection();
    $rankings = $connection->getRankings();

    $rankingsList = array();
    $players = array();

    foreach ($rankings as $player) {
        array_key_exists($player['idplayer'], $players) ?: $players[$player['idplayer']] = $player['name'];
    }

    natcasesort($players);
    $playerKeys = array_keys($players);

    foreach ($rankings as $ranking) {
        if (!isset($rankingsList[$ranking['idcard']])) {
            $rankingsList[$ranking['idcard']]['idcard'] = $ranking['idcard'];
            $rankingsList[$ranking['idcard']]['cardName'] = $ranking['cardName'];
            $rankingsList[$ranking['idcard']]['scryID'] = $ranking['scryID'];
            $rankingsList[$ranking['idcard']]['betID'] = $ranking['idbet'];
            $rankingsList[$ranking['idcard']]['doubleFaced'] = $ranking['doubleFaced'];
            $rankingsList[$ranking['idcard']]['rankings'] = array_fill_keys($playerKeys, "");
        }
        //array_push($rankingsList[$ranking['idcard']]['rankings'], array('id' => $ranking['idplayer'], 'ranking' => $ranking['rankingValue']));
        $rankingsList[$ranking['idcard']]['rankings'][$ranking['idplayer']] = $ranking['rankingValue'];
    }
    ?>

    <h2>View Rankings</h2>
    <h6>Set: <?php echo strtoupper(DbConfig::ACTIVE_SET); ?> </h6>
    <div>
        <table class="table table-striped">
            <thead>
                <tr>
                    <th class='sticky-top sticky-offset'>Card</th>
                    <?php
                    echo "<th class='sticky-top sticky-offset'></th>"; //Bet Column
                    foreach ($players as $player) {
                        echo "<th class='sticky-top sticky-offset'>${player}</td>";
                    }
                    ?>
                </tr>
            </thead>
            <tbody>
                <?php
                foreach ($rankingsList as $card) {
                    $cardMin = min(array_diff($card['rankings'], array(null)));
                    $cardMax = max($card['rankings']);

                    echo ($cardMax - $cardMin) >= 1.5 ? "<tr class='highlight'>" : "<tr>";
                    echo "<td><a class='mtg-card' title='Click to Preview'"
                    . "data-img='https://c1.scryfall.com/file/scryfall-cards/border_crop/front/" . $card['scryID'][0] . "/" . $card['scryID'][1] . "/" . $card['scryID'] . ".jpg' ";
                    echo $card['doubleFaced'] ? "data-img2='https://c1.scryfall.com/file/scryfall-cards/border_crop/back/" . $card['scryID'][0] . "/" . $card['scryID'][1] . "/" . $card['scryID'] . ".jpg' " : "";
                    echo " >${card['cardName']}</a>";
                    echo ($card['betID']) ? "<img class='ml-s' src='img/gold-coin.png'></td>" : "</td>";
                    echo "<td><a><button type='button' class='btn btn-primary' data-cardid='" . $card['idcard'] . "' data-low='${cardMin}' data-high='${cardMax}'>Bet</button></a></td>";
                    foreach ($card['rankings'] as $ranking) {
                        echo (($cardMax - $cardMin) >= 1.5 && ($ranking == $cardMin || $ranking == $cardMax)) ? "<td><strong>${ranking}</strong></td>" : "<td>${ranking}</td>";
                    }
                }
                echo "</tr>";
                ?>
            </tbody>
        </table>
    </div>
</div>

<div id="cardViewer" class="modal fade" tabindex="-1" role="dialog" aria-labelledby="Card Viewer" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-body">
            </div>
        </div>
    </div>
</div>

<div id="betViewer" class="modal fade" tabindex="-1" role="dialog" aria-labelledby="Bet Viewer" aria-hidden="true">
    <div class="modal-dialog modal-lg">
        <div class="modal-content">
            <div class="modal-body row">
                <div class="bet-card col-6">

                </div>
                <div class="bet-data col-6">
                    <form role="form" id="submit-bet-form">
                        <label class="p-2">Low Bet</label>
                        <input type="text" class="form-control" id="low-bet" placeholder="Low bet" required>
                        <select class="form-control" id="low-side" multiple>
                            <?php
                            foreach ($players as $key => $player) {
                                echo "<option value='${key} '>${player}</option>";
                            }
                            ?>
                        </select>
                        <label class="p-2">High Bet</label>
                        <input type="text" class="form-control" id="high-bet" placeholder="High bet" required>
                        <select class="form-control" id="high-side" multiple>
                            <?php
                            foreach ($players as $key => $player) {
                                echo "<option value='${key} '>${player}</option>";
                            }
                            ?>
                        </select>
                        <div id="feedback" role="alert"></div>
                        <input type="hidden" id="cardid">
                        <button type="submit" class="btn btn-primary mt-2" id="submit">Submit</button>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>


<script type="text/javascript">
    $(document).ready(function () {
        //Pop Card Viewer
        $('.mtg-card').on('click', function () {
            if ($(this).attr('data-img2')) {
                $('#cardViewer .modal-dialog').addClass('modal-lg');
                $('#cardViewer .modal-body').html("<img src='" + $(this).attr('data-img') + "' width='50%'>");
                $('#cardViewer .modal-body').append("<img src='" + $(this).attr('data-img2') + "' width='50%'>");
            } else {
                $('#cardViewer .modal-dialog').removeClass('modal-lg');
                $('#cardViewer .modal-body').html("<img src='" + $(this).attr('data-img') + "' width='100%'>");
            }
            $('#cardViewer').modal('show');
        });
        //Pop Bet Maker
        $('td button').on('click', function () {

            var front = $(this).parents('tr').find('a').attr('data-img');
            var back = $(this).parents('tr').find('a').attr('data-img2');

            $('#cardid').val($(this).attr('data-cardid'));
            $('#low-bet').val($(this).attr('data-low'));
            $('#high-bet').val($(this).attr('data-high'));

            $("#low-side option:selected").prop("selected", false);
            $("#high-side option:selected").prop("selected", false);

            $('#betViewer .modal-body .bet-card').html("<img src='" + front + "' width='100%'>");
            if (back) {
                $('#betViewer .modal-body .bet-card').append("<img src='" + back + "' width='100%'>");
            }
            $('#betViewer').modal('show');
        });
        //Submit Bet
        var submitted = false;
        $('#submit-bet-form').on('submit', function () {

            var data = {service: 'SUBMIT_BET'};
            data.cardID = $('#cardid').val();
            data.lowBet = $('#low-bet').val();
            data.highBet = $('#high-bet').val();
            data.lowSide = $("#low-side").val();
            data.highSide = $("#high-side").val();

            if (!submitted) {
                //submitted = true;

                $.ajax({
                    type: 'POST',
                    data: data,
                    url: './js/ajax-handler.php',
                    dataType: 'json',
                    error: function (jqXHR, textStatus, errorThrown) {},
                    success: function (response, textStatus, jqXHR) {
                        if (response.error) {
                            alert(response.error);
                            submitted = false;
                        } else {
                            $('#betViewer').modal('hide');
                            $('a').find('[data-cardid="' + data.cardID + '"]').parents('tr').find('.mtg-card').after("<img class='ml-2' src='img/gold-coin.png'>");
                            submitted = false;
                        }
                    },
                    complete: function (jqXHR, textStatus) {}
                });

            }
            return false;

        });

    });
</script>
<?php include_once "./footer.php" ?>