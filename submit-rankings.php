<?php $_COOKIE['playerID'] ? $playerIDcookie = $_COOKIE['playerID'] : "" ?>
<?php include_once "./header.php" ?>
<?php include_once $_SERVER['DOCUMENT_ROOT'] . "/mtg_bets/app/SQLConnection.php"; ?>

<div class="container my-2 col-12">
    <h2>Submit Rankings</h2>
    <form role="form" id="submit-rankings-form">
        <div class="form-row sticky-top sticky-offset">
            <div class="col-6">
                <select class="form-control" id="playerID" name="playerID" required>
                    <?php
                    $connection = new SQLConnection();

                    $results = $connection->getPlayers();
                    
                    $playerFound = false;
                    if(isset($playerIDcookie)){
                        foreach ($results as $player) {
                            $playerFound = $playerIDcookie == $player ? true : false;
                        }
                    }
                    echo !isset($playerIDcookie) && !$playerFound ? '<option selected disabled value="0">Player</option>' :"";

                    foreach ($results as $player) {
                        echo "<option value=" . $player['idplayer'];
                        echo isset($playerIDcookie) && $playerIDcookie == $player['idplayer'] ? " selected " : "";
                        echo ">" . $player['name'] . "</option>";
                    }
                    ?>
                </select>
            </div>
            <div class="col-6">
                <button type="submit" class="btn btn-primary" id="submit">Save Rankings</button>
            </div>
            <div id="feedback" role="alert" class="col-12"></div>
        </div>

        <div class="row">
            <?php
            $cards = $connection->getCardIds();
            foreach ($cards as $card) {
                echo "<div class='col-2 my-2 mtg-card'>";
                echo "<img src=https://c1.scryfall.com/file/scryfall-cards/border_crop/front/" . $card['scryID'][0] . "/" . $card['scryID'][1] . "/" . $card['scryID'] . ".jpg>";
                if ($card['doubleFaced']) {
                    echo "<img class='d-none'src=https://c1.scryfall.com/file/scryfall-cards/border_crop/back/" . $card['scryID'][0] . "/" . $card['scryID'][1] . "/" . $card['scryID'] . ".jpg>";
                }
                echo "<input type='text' id='" . $card['idcard'] . "' class='form-control' placeholder='ranking'>";
                echo "</div>";
            }
            ?>
        </div>
    </form>
</div>

<script type="text/javascript">
    $(document).ready(function () {
        var submitted = true;

        //Toggle front/back faces of card images
        $('.mtg-card').each(function () {
            if ($(this).children('img').length === 2) {
                $(this).click(function () {
                    $(this).children('img').each(function () {
                        if ($(this).hasClass('d-none')) {
                            $(this).removeClass('d-none');
                        } else {
                            $(this).addClass('d-none');
                        }
                    });
                });
            }
            ;
        });

        //Validate input
        var regex = /^[0-5]$|^[0-4]{1}\.[05]$|^5\.0$|^$/;
        $('.mtg-card input').change(function () {
            if (regex.test($(this).val())) {
                $(this).removeClass("is-invalid");
                this.setCustomValidity("");
                submitted = false; //they made an edit
                var wholeNumber = /^[0-5]$/;
                if (wholeNumber.test($(this).val())) {
                    $(this).val($(this).val().concat(".0"));
                }
            } else {
                $(this).addClass("is-invalid");
                this.setCustomValidity("Enter a valid ranking boomer.");
            }
        });

        //Saving warning
        $(window).bind('beforeunload', function () {
            if (!submitted) {
                return "Leave without saving?";
            } else {
                return;
            }
        });

        //Load Rankings
        $('#playerID').change(function () {
            if (this.value > 0) {
                var data = {service: 'GET_RANKINGS', playerID: this.value};
                $.ajax({
                    type: 'POST',
                    data: data,
                    url: './js/ajax-handler.php',
                    dataType: 'json',
                    error: function (jqXHR, textStatus, errorThrown) {},
                    success: function (response, textStatus, jqXHR) {
                        if (response.error) {
                            alert(response.error);
                        } else {
                            $(".mtg-card input").val(""); //clear rankings

                            jQuery.each(response, function () {
                                $("#" + this.card_idcard).val(this.rankingValue);
                            });

                        }
                    },
                    complete: function (jqXHR, textStatus) {}
                });
            }
        });

        //Submit Rankings
        $('#submit-rankings-form').on('submit', function () {

            if ($('.mtg-card input .is-invalid').length) {
                $('#feedback').html("You have invalid rankings.");
                $('#feedback').addClass("mt-2 alert alert-danger");
                return false;
            }

            if ($('#playerID').val() < 1) {
                $('#feedback').html("You must select a player.");
                $('#feedback').addClass("mt-2 alert alert-danger");
                return false;
            }


            //clear warning
            $('#feedback').html("");
            $('#feedback').removeClass("mt-2 alert alert-danger");

            var data = {service: 'SUBMIT_RANKINGS', playerID: $('#playerID').val(), cards: []};

            $('.mtg-card input').each(function () {
                var card = {cardID: $(this).attr('id'), ranking: $(this).val()};
                data.cards.push(card);
            });

            if (!submitted) {
                submitted = true;

                $.ajax({
                    type: 'POST',
                    data: data,
                    url: './js/ajax-handler.php',
                    dataType: 'json',
                    error: function (jqXHR, textStatus, errorThrown) {},
                    success: function (response, textStatus, jqXHR) {
                        if (response.error) {
                            alert(response.error);
                            submitted = false;
                        } else {
                            if (response > 0) {
                                alert("Saved!");
                            }
                        }
                    },
                    complete: function (jqXHR, textStatus) {}
                });

            }
            return false;

        });
<?php echo isset($_COOKIE['playerID']) ? "$('#playerID').trigger('change');" : "" ?>
    });
</script>
<?php include_once "./footer.php" ?>